# Idntify

## Requirements

* Node.js >= 14.17.4
* npm >= 7.20.3

## Before start

* Install dependencies: `npm install`

## DB

For this initial project I suggest three tables: `commerces`, `users` and `transactions`. Below is the entity relationship diagram for the DB:

![ERD for DB](resources/erd_db.png "ERD for DB")

### Setup DB

This project includes an utility to initialize a DB with a defined schemas. To initialize DB run:

`npm run db:migrate`

## API

For API was choosed [Fastify](https://fastify.io/) because it's a really fast framework. Probably it's a very new framework to work with it but it's developing for some core Node.js members with a lot of experience in JavaScript and Node.js.

Fastify has a very [awesome performance](https://www.fastify.io/benchmarks/) handle a high number of requests per second. Even if Fastify it's newer has a lot of *plugins* to extend their functionality or use it with another technologies.

In our case we choose these plugins:

* [`fastify-swagger`](https://github.com/fastify/fastify-swagger) is used to automatically generate a great page with all information about our services
* [`fastify-postgres`](https://github.com/fastify/fastify-postgres) is used to easily access to a posgres connection and can interact with our db
* [`fluent-json-schema`](https://github.com/fastify/fluent-json-schema) is used to generate JSON Schemas in a more comprehensive way. These schemas was used by fastify to validate data received through the endpoints

### Run API

It's very simple run REST API as run this command: `npm run start:services` and API will run on port 3001.

**NOTE**: This API includes an OpenAPI Spec (aka OAS) and you can review this documentation on http://localhost:3001/docs

## Dashboard

For dashboard was choosed [React](https://reactjs.org/) because it's a very powerful framework developed by Facebook with a very large and strong community that support and improve it frequently. As well a lot of packages and tutorials were developed around React so it's very easy to found information and tools to speed-up the development.

___

`material-ui` is the choose for layout and base components because it's based on Material Desing the Google's desing framework.

`chartjs` was choosen for data visualization because it's a very simple but powerful tool.

___

### Run dashboard

It's very simple run dashboard as run this command: `npm run start:dashboard` and React App will run on port 3000.

## What's next:

* Create a js script to insert into eCommerces to detect automatically what type of gateway are used and collect transactions data
* Move DB to AWS RDS
* Create at least one the following: *Shopify App*, *Woocomerce plugin* or *Magento plugin*
* Create a CI/CD pipeline using Gitlab CI
* Create a new package to automate infrastructure using a tool like [Terraform](https://www.terraform.io/)