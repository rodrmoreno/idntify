
exports.up = (knex) =>
  knex.schema
    .createTable('transactions', (table) => {
      /** Fields the */
      table.increments('id')
      table.integer('commerce_id').unsigned().references('id').inTable('commerces')
      table.string('customer_email', 255)
      table.string('customer_phone', 13)
      table.integer('user_id').unsigned().references('id').inTable('customers')
      table.boolean('is_approved', false).notNullable()
      table.enum('gateway', ['conekta', 'openpay', 'stripe']).notNullable()
      table.string('device_session_id', 255).notNullable()
      table.string('user_agent', 255)
      table.string('customer_ip', 39).notNullable()
      table.biginteger('amount').unsigned().notNullable()
      table.string('currency', 3).notNullable()
      table.string('card_token', 255).notNullable()
      table.timestamps(true)

      /** Indexes */
    })

exports.down = (knex) =>
  knex.schema
    .dropTable('transactions')
