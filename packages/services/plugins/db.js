'use strict'

const fp = require('fastify-plugin')

/**
 * This plugins adds some utilities to use postgres
 *
 * @see https://github.com/fastify/fastify-postgres
 */
module.exports = fp(async function (fastify, _opts) {
  fastify.register(require('fastify-postgres'), {
    connectionString: 'postgres://postgres:postgress@localhost/postgres'
  })
})
