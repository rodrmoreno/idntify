'use strict'

module.exports = async function (fastify, _opts) {
    fastify.post('/', async (request, reply) => {
        const { name, legalName, rfc, address, state, country, zipCode } = request.body

        try {
            const client = await fastify.pg.connect()
            const { rows } = await client.query(
                'insert into commerces(name, legal_name, rfc, address, state, country, zip_code) values ($1, $2, $3, $4, $5, $6, $7) returning *',
                [name, legalName, rfc, address, state, country, zipCode]
            )
            client.release()

            reply.code(201)
            return rows[0]
        } catch (err) {
            throw err
        }
    })

    fastify.get('/', async (_request, _reply) => {
        try {
            const client = await fastify.pg.connect()
            const { rows } = await client.query('select * from commerces')
            client.release()

            return rows
        } catch (err) {
            throw err
        }
    })

    fastify.get('/:id', async (request, reply) => {
        try {
            const client = await fastify.pg.connect()
            const { rows } = await client.query('select * from commerces where id = $1', [request.params.id])
            client.release()

            if (!rows.length) {
                reply.code(404)
                return {}
            }

            return rows[0]
        } catch (err) {
            throw err
        }
    })

    // TODO: FIXME
    fastify.patch('/:id', async (request, reply) => {
        try {
            const client = await fastify.pg.connect()
            await client.query('update commerces  where id = $1', [request.params.id])

            reply.code(204)
            return {}
        } catch (err) {
            throw err
        }
    })

    fastify.delete('/:id', async (request, reply) => {
        try {
            const client = await fastify.pg.connect()
            await client.query('delete from commerces where id = $1', [request.params.id])

            reply.code(204)
            return {}
        } catch (err) {
            throw err
        }
    })
}