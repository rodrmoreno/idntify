'use strict'

const S = require('fluent-json-schema').default

module.exports = S.object()
  .title('Transaction request JSON Schema')
  .description('A simple transaction')
  .additionalProperties(false)
  .prop('customer', S.object()
    .additionalProperties(false)
    .prop('email', S.string(S.FORMATS.EMAIL))
    .prop('phone', S.string().maxLength(13))
    .prop('ip', S.string(S.FORMATS.IPV4).required())
  )
  .prop('isApproved', S.boolean().required())
  .prop('gateway', S.enum(['conekta', 'openpay', 'stripe']).required())
  .prop('deviceSessionId', S.string().maxLength(255).required())
  .prop('userAgent', S.string().maxLength(255))
  .prop('amount', S.integer().required())
  .prop('currency', S.string().minLength(3).maxLength(3).required())
  .prop('cardToken', S.string().maxLength(255).required())
