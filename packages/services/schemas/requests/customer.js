'use strict'

const S = require('fluent-json-schema').default

module.exports = S.object()
  .title('Customer request JSON Schema')
  .description('A simple customer')
  .additionalProperties(false)
  .prop('firstName', S.string().maxLength(20).required())
  .prop('lastName', S.string().maxLength(20).required())
  .prop('email', S.string(S.FORMATS.EMAIL).required())
  .prop('phone', S.string().maxLength(13))
