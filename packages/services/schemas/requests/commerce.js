'use strict'

const S = require('fluent-json-schema').default

module.exports = S.object()
  .title('Commerce request JSON Schema')
  .description('A simple commerce')
  .additionalProperties(false)
  .prop('name', S.string().maxLength(50).required())
  .prop('legalName', S.string().maxLength(100).required())
  .prop('rfc', S.string().maxLength(13).required())
  .prop('address', S.string().maxLength(100))
  .prop('state', S.string().maxLength(20))
  .prop('country', S.string().minLength(2).maxLength(2))
  .prop('zipCode', S.string().minLength(5).maxLength(5))
