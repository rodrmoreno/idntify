'use strict'

const S = require('fluent-json-schema').default

module.exports = S.object()
  .title('Transaction request JSON Schema')
  .description('A simple transaction')
  .additionalProperties(false)
  .prop('id', S.integer().required())
