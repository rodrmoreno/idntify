'use strict'

const requests = require('./requests')
const responses = require('./responses')

/**
 * @endpoint POST /transactions/create
 *
 * Endpoint for create a new record of transaction
 */
exports.postTransaction = {
  schema: {
    body: requests.transaction,
    response: {
      201: responses.transaction
    }
  }
}
